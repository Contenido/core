create sequence mailqueue_id_seq;
select setval('mailqueue_id_seq', 1, true);

create table mailqueue
(
	id integer not null primary key default nextval('public.mailqueue_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	etime timestamp,
	status smallint not null default 0,
	sections integer,
	name text,
	subject text,
	data text
);
create index mailqueue_actual on mailqueue (status, dtime) WHERE status = 0;
create index mailqueue_dtime on mailqueue (dtime);
