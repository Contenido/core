package Contenido::Accessor;

sub mk_accessors {
    my ($class, @names) = @_;
		
    foreach my $name (@names) {
	no strict 'refs';
	*{"${class}::$name"} = sub {
		my $self = shift;
		if ( @_ ) {
			return $self->{$name} = shift;
		} else {
			return $self->{$name};
		}
	};
    }
}

1;