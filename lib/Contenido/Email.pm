package Contenido::Email;

use strict;
use base 'Contenido::Document';
use Contenido::Globals;
use SQL::MailQueueTable;

sub extra_properties
{
        return (
		{ 'attr' => 'status',
			'cases' => [
				[0, 'Ожидает доставки'],
				[1, 'Доставлен'],
				[2, 'Обработка'],
				[3, 'Ошибка'],
				[-1, 'Удален'],
			],
		},
		{ 'attr' => 'cc',		'type' => 'text',	'rusname' => 'Дополнительные получатели (по одному на строку)', rows => 10 },
		{ 'attr' => 'from',		'type' => 'string',	'rusname' => 'Адрес отправителя' },
		{ 'attr' => 'body_text',	'type' => 'text',	'rusname' => 'Текст письма (txt)', 'rows' => 40 },
		{ 'attr' => 'body_html',	'type' => 'wysiwyg',	'rusname' => 'Текст письма (html)', 'rows' => 40 },
		{ 'attr' => 'error',		'type' => 'text',	'rusname' => 'Сообщение об ошибке', 'rows' => 5 },
		{ 'attr' => 'pictures',		'type' => 'images',	'rusname' => 'Список иллюстраций', preview => ['400x400'], crop => ['870x450','85x85'] },
	)
}


sub class_name
{
	return 'E-mail';
}

sub class_description
{
	return 'E-mail сообщение';
}

sub contenido_status_style
{
	my $self = shift;
	if ( $self->status == 2 ) {
		return 'color:green;';
	} elsif ( $self->status == 3 ) {
		return 'color:green;';
	} elsif ( $self->status < 0 ) {
		return 'text-decoration:line-through;';
	}
}

sub class_table
{
	return 'SQL::MailQueueTable';
}

sub search_fields {
	return ('name');
}

sub pre_store
{
	my $self = shift;
	my $default_section = $project->s_alias->{spam}	if ref $project->s_alias && exists $project->s_alias->{spam};
	if ( $default_section ) {
		$self->sections($default_section);
	}

	return 1;
}

1;
